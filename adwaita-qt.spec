%undefine __cmake_in_source_build

Name:          adwaita-qt
Version:       1.4.2
Release:       3
License:       GPL-2.0 and GPL-2.0+
Summary:       Adwaita theme for Qt-based applications
Url:           https://github.com/FedoraQt/adwaita-qt
Source0:       https://github.com/FedoraQt/adwaita-qt/archive/refs/tags/%{version}.tar.gz

BuildRequires: cmake libxcb-devel

%description
A native style to bend Qt5/Qt6 applications to look like they belong into GNOME Shell.

%package -n adwaita-qt5
Summary:        Adwaita Qt5 theme
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtx11extras-devel

Requires:       libadwaita-qt5%{?_isa} = %{version}-%{release}

%description -n adwaita-qt5
Adwaita theme variant for applications utilizing Qt5.

%package -n libadwaita-qt5
Summary:        Adwaita Qt5 library

%description -n libadwaita-qt5
%{summary}.

%package -n libadwaita-qt5-devel
Summary:        Development files for libadwaita-qt5
Requires:       libadwaita-qt5%{?_isa} = %{version}-%{release}

%description -n libadwaita-qt5-devel
The libadwaita-qt5-devel package contains libraries and header files for
developing applications that use libadwaita-qt5.

%package -n adwaita-qt6
Summary:        Adwaita Qt6 theme
BuildRequires:  qt6-qtbase-devel

Requires:       libadwaita-qt6%{?_isa} = %{version}-%{release}

%description -n adwaita-qt6
Adwaita theme variant for applications utilizing Qt6.

%package -n libadwaita-qt6
Summary:        Adwaita Qt6 library

%description -n libadwaita-qt6
%{summary}.

%package -n libadwaita-qt6-devel
Summary:        Development files for libadwaita-qt6
Requires:       libadwaita-qt6%{?_isa} = %{version}-%{release}

%description -n libadwaita-qt6-devel
The libadwaita-qt6-devel package contains libraries and header files for
developing applications that use libadwaita-qt6.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%global _vpath_builddir %{_target_platform}-qt5
%cmake -DQT_PLUGINS_DIR=%{_libdir}/qt5/plugins
%cmake_build

%global _vpath_builddir %{_target_platform}-qt6
%cmake -DUSE_QT6=true
%cmake_build

%install
%global _vpath_builddir %{_target_platform}-qt5
%cmake_install

%global _vpath_builddir %{_target_platform}-qt6
%cmake_install

rm -rf %{buildroot}%{_libdir}/pkgconfig/adwaita-qt6.pc

%files -n adwaita-qt5
%doc README.md
%license LICENSE.LGPL2
%{_qt5_plugindir}/styles/adwaita.so

%files -n libadwaita-qt5
%{_libdir}/libadwaitaqt.so.*
%{_libdir}/libadwaitaqtpriv.so.*

%files -n libadwaita-qt5-devel
%dir %{_includedir}/AdwaitaQt
%{_includedir}/AdwaitaQt/*.h
%dir %{_libdir}/cmake/AdwaitaQt
%{_libdir}/cmake/AdwaitaQt/*.cmake
%{_libdir}/pkgconfig/adwaita-qt.pc
%{_libdir}/libadwaitaqt.so
%{_libdir}/libadwaitaqtpriv.so

%files -n adwaita-qt6
%doc README.md
%license LICENSE.LGPL2
%{_qt6_plugindir}/styles/adwaita.so

%files -n libadwaita-qt6
%{_libdir}/libadwaitaqt6.so.*
%{_libdir}/libadwaitaqt6priv.so.*

%files -n libadwaita-qt6-devel
%dir %{_includedir}/AdwaitaQt6
%{_includedir}/AdwaitaQt6/*.h
%dir %{_libdir}/cmake/AdwaitaQt6
%{_libdir}/cmake/AdwaitaQt6/*.cmake
%{_libdir}/libadwaitaqt6.so
%{_libdir}/libadwaitaqt6priv.so


%changelog
* Fri Dec 27 2024 Oxyan <oxyan70@gmail.com> - 1.4.2-3
- fix build error

* Thu Nov 21 2024 Funda Wang <fundawang@yeah.net> - 1.4.2-2
- adopt to new cmake macro

* Tue Nov 21 2023 lwg <liweiganga@uniontech.com> - 1.4.2-1
- update to version 1.4.2

* Fri Jul 07 2023 xu_ping <707078654@qq.com> - 1.1.0-7
- Modify changelog release version

* Thu Dec 03 2020 Ge Wang <wangge20@huawei.com> - 1.1.0-6
- Modify license

* Tue Apr 21 2020 chengzihan <chengzihan2@huawei.com> - 1.1.0-5
- Package init
